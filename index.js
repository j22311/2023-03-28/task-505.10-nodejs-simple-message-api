//omport thư viện express tương đương import exprerss from "express"
const express = require("express");

//khỡi tạo một app express
const app = express();

//khai báo cổng chạy project
const port = 8000;
//callback function là một function đóng vai trò là tham số của một function khác
//khai báo API dạng/
app.get("/",(req,res) => {
    let today = new Date();

    res.status(200).json({
        message: `xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()} `
    })
})
//KHai báo dạng API dạng GET
app.get("/get-method",(req,res) => {
    res.json({
        message: "GET Method"
    })
})
//KHai báo dạng API dạng POST
app.post("/post-method",(req,res) => {
    res.json({
        message: "POST Method"
    })
})

//KHai báo dạng API dạng PUT
app.put("/put-method",(req,res) => {
    res.json({
        message: "PUT Method"
    })
})

//KHai báo dạng API dạng DELETE
app.delete("/delete-method",(req,res) => {
    res.json({
        message: "DELETE Method"
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})